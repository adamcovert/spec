$(document).ready(function () {

  $(window).scroll(function () {
    windowScroll();
  }).scroll();

  function windowScroll() {
    var pageScrollY = 0;
    if (typeof(window.pageYOffset) == 'number') pageScrollY = window.pageYOffset;
    else pageScrollY = document.documentElement.scrollTop;

    if (pageScrollY > 20) {
      $('.s-page').addClass('s-page--is-scrolling');
    } else {
      $('.s-page').removeClass('s-page--is-scrolling');
    }
  }

  $('.s-all-services__show-more').on('click', function() {
    var button = $(this);
    var text = button.text() == 'Показать больше услуг' ? 'Показать меньше услуг' : 'Показать больше услуг';
    button.text(text);

    $('.s-all-services__list').each(function() {
      $(this).toggleClass('s-all-services__list--is-active');
    });
  });

  $('.s-hamburger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-open');
    $('body').addClass('mobile-menu-is-open');
    document.body.style.position = 'fixed';
    document.body.style.top = `-${window.scrollY}px`;
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-open');
    $('body').removeClass('mobile-menu-is-open');
    const scrollY = document.body.style.top;
    document.body.style.position = '';
    document.body.style.top = '';
    window.scrollTo(0, parseInt(scrollY || '0') * -1);
  });

  var testimonialsSlider = new Swiper('.s-testimonials__slider', {
    slidesPerView: 2,
    spaceBetween: 30,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      767: {
        slidesPerView: 1,
        spaceBetween: 15
      }
    }
  });

  $('.s-testimonials--gallery').lightGallery({
    selector: '.s-testimonial-card__document',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });
});